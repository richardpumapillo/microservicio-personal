package com.softconsis.microservicio.app.personal.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "capacitaciones")
public class Capacitacion implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected long id;

    @Column(name = "tomo")
    protected String tomo;

    @Column(name = "folio")
    protected String folio;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fecha_inicio")
    protected Date fechaInicio;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fecha_fin")
    protected Date fechaFin;

    @JoinColumn(name = "centros_educativos_id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    protected CentroEducativo centroEducativo;

    @JoinColumn(name = "tipos_capacitacion_id", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    protected TipoCapacitacion tipoCapacitacion;

    public Capacitacion() {

    }

    public Capacitacion(String tomo, String folio, Date fechaInicio, Date fechaFin, CentroEducativo centroEducativo, TipoCapacitacion tipoCapacitacion) {
        this.tomo = tomo;
        this.folio = folio;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.centroEducativo = centroEducativo;
        this.tipoCapacitacion = tipoCapacitacion;
    }

    public long getId() {
        return id;
    }

    public String getTomo() {
        return tomo;
    }

    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

	public CentroEducativo getCentroEducativo() {
		return centroEducativo;
	}

	public void setCentroEducativo(CentroEducativo centroEducativo) {
		this.centroEducativo = centroEducativo;
	}

    public TipoCapacitacion getTipoCapacitacion() {
        return tipoCapacitacion;
    }

    public void setTipoCapacitacion(TipoCapacitacion tipoCapacitacion) {
        this.tipoCapacitacion = tipoCapacitacion;
    }
}
