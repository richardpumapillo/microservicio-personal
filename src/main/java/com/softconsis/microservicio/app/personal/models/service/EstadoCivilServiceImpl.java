package com.softconsis.microservicio.app.personal.models.service;

import com.softconsis.microservicio.app.personal.models.dao.EstadoCivilDao;
import com.softconsis.microservicio.app.personal.models.entity.EstadoCivil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EstadoCivilServiceImpl implements IEstadoCivilService{

    @Autowired
    private final EstadoCivilDao estadoCivilDao;

    public EstadoCivilServiceImpl(EstadoCivilDao estadoCivilDao) {
        this.estadoCivilDao = estadoCivilDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EstadoCivil> findAll() {
        return (List<EstadoCivil>)estadoCivilDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public EstadoCivil findById(Long id) {
        return estadoCivilDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public EstadoCivil save(EstadoCivil estadoCivil) {
        return estadoCivilDao.save(estadoCivil);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        estadoCivilDao.deleteById(id);
    }
}
