package com.softconsis.microservicio.app.personal.models.service;

import com.softconsis.microservicio.app.personal.models.entity.Empleado;

import java.util.List;

public interface IEmpleadoService {
    public List<Empleado> findAll();
    public Empleado findById(Long id);
    public Empleado save(Empleado empleado);
    public void deleteById(Long id);
}
