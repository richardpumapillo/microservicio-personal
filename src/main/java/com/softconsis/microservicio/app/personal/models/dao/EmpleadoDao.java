package com.softconsis.microservicio.app.personal.models.dao;

import com.softconsis.microservicio.app.personal.models.entity.Empleado;
import org.springframework.data.repository.CrudRepository;

public interface EmpleadoDao extends CrudRepository<Empleado,Long> {
}
