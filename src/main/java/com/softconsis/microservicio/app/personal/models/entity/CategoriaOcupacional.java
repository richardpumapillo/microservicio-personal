package com.softconsis.microservicio.app.personal.models.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "categorias_ocupacionales")
public class CategoriaOcupacional implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "descripcion")
    protected String descripcion;


}
