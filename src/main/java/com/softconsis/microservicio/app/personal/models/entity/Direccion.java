package com.softconsis.microservicio.app.personal.models.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Direccion {
    @Column(name = "calle")
    protected String calle;

    @Column(name = "distrito")
    protected String distrito;

    @Column(name = "provincia")
    protected String provincia;

    @Column(name = "departamento")
    protected String departamento;

    public Direccion(){}

    public Direccion(String calle, String distrito, String provincia, String departamento) {
        this.calle = calle;
        this.distrito = distrito;
        this.provincia = provincia;
        this.departamento = departamento;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
}
