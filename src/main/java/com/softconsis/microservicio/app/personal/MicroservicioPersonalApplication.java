package com.softconsis.microservicio.app.personal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@EntityScan({"com.softconsis.microservicio.app.commons.models.entity","com.softconsis.microservicio.app.personal.models.entity"})
@SpringBootApplication
public class MicroservicioPersonalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicioPersonalApplication.class, args);
	}

}
