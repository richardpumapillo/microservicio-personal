package com.softconsis.microservicio.app.personal.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tipos_escolaridad")
public class TipoEscolaridad implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "nivel")
    protected String nivel;

    @OneToMany(mappedBy = "tipoEscolaridad", cascade = CascadeType.ALL)
    protected Set<Escolaridad> escolaridades;

    public TipoEscolaridad() {
        this.escolaridades = new HashSet<Escolaridad>();
    }

    public TipoEscolaridad(String nivel) {
        this.nivel = nivel;
        this.escolaridades = new HashSet<Escolaridad>();
    }

    public Long getId() {
        return id;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public Set<Escolaridad> getEscolaridades() {
        return escolaridades;
    }

    public void setEscolaridades(Set<Escolaridad> escolaridades) {
        this.escolaridades = escolaridades;
    }
}
