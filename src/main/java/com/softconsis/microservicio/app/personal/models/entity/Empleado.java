package com.softconsis.microservicio.app.personal.models.entity;

import org.hibernate.annotations.GenerationTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "empleados")
public class Empleado implements Serializable{

    private static final long serialVersionUID = 109361099674524889L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected Long id;

    public Long getId() {
        return id;
    }

    @NotNull
    @Column(name = "nombre", nullable = false)
    protected String nombre;

    @NotNull
    @Column(name = "apellido_paterno", nullable = false)
    protected String apellidoPaterno;

    @NotNull
    @Column(name = "apellido_materno", nullable = false)
    protected String apellidoMaterno;

    @NotNull
    @Column(name = "dni", nullable = false, length = 8)
    protected String dni;

    @NotNull
    @Column(name = "sexo", nullable = false, length = 1)
    protected String sexo;

    @Embedded
    protected Direccion direccionCasa;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "calle", column = @Column(name = "nacimiento_calle")),
            @AttributeOverride(name = "distrito", column = @Column(name = "nacimiento_distrito")),
            @AttributeOverride(name = "provincia", column = @Column(name = "nacimiento_provincia")),
            @AttributeOverride(name = "departamento", column = @Column(name = "nacimiento_departamento"))
    })
    protected Direccion direccionNacimiento;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "last_update", insertable = false,updatable = true)
    @org.hibernate.annotations.Generated(GenerationTime.ALWAYS)
    protected Date lastUpdated;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "created_on", updatable = false)
    @org.hibernate.annotations.CreationTimestamp
    protected Date createdOn;
    
    @Transient
    protected Integer port;

    /*
    @JoinColumn(name = "estados_civiles_id", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    protected EstadoCivil estadoCivil;

    @JoinColumn(name = "escolaridades_id", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    protected Escolaridad escolaridad;
    */

    @PrePersist
    public void prePersist() {
        createdOn = new Date();
    }

    public Empleado() {
    }

    public Empleado(String nombre, String apellidoPaterno, String apellidoMaterno, String dni, String sexo, Direccion direccionCasa, Direccion direccionNacimiento) {
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.dni = dni;
        this.sexo = sexo;
        this.direccionCasa = direccionCasa;
        this.direccionNacimiento = direccionNacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Direccion getDireccionCasa() {
        return direccionCasa;
    }

    public void setDireccionCasa(Direccion direccionCasa) {
        this.direccionCasa = direccionCasa;
    }

    public Direccion getDireccionNacimiento() {
        return direccionNacimiento;
    }

    public void setDireccionNacimiento(Direccion direccionNacimiento) {
        this.direccionNacimiento = direccionNacimiento;
    }

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
    
    
}
