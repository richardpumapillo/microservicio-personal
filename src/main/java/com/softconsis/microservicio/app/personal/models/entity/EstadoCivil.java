package com.softconsis.microservicio.app.personal.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "estados_civiles")
public class EstadoCivil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "nombre_estado")
    protected String nombreEstado;

    /*
    @OneToMany(mappedBy = "estadoCivil",cascade = CascadeType.ALL)
    protected Set<Empleado> empleados;*/

    public EstadoCivil() {
        //this.empleados = new HashSet<Empleado>();
    }

    public EstadoCivil(String nombreEstado) {
        this.nombreEstado = nombreEstado;
        //this.empleados = new HashSet<Empleado>();
    }

    public Long getId() {
        return id;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }
/*
    public Set<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Set<Empleado> empleados) {
        this.empleados = empleados;
    }
*/
}

