package com.softconsis.microservicio.app.personal.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "especialidades")
public class Especialidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "descripcion")
    protected String descripcion;

    @OneToMany(mappedBy = "especialidad", cascade = CascadeType.ALL)
    protected Set<Escolaridad> escolaridades;

    public Especialidad() {
        this.escolaridades = new HashSet<Escolaridad>();
    }

    public Especialidad(String descripcion) {
        this.descripcion = descripcion;
        this.escolaridades = new HashSet<Escolaridad>();
    }

    public Long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set<Escolaridad> getEscolaridades() {
        return escolaridades;
    }

    public void setEscolaridades(Set<Escolaridad> escolaridades) {
        this.escolaridades = escolaridades;
    }
}
