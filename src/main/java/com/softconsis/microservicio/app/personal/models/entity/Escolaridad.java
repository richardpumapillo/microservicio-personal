package com.softconsis.microservicio.app.personal.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "escolaridades")
public class Escolaridad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_graduacion")
    protected Date fechaGraduacion;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "tipos_escolaridad_id", nullable = false)
    protected TipoEscolaridad tipoEscolaridad;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "especialidades_id", nullable = false)
    protected Especialidad especialidad;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "centros_educativos_id", nullable = false)
    protected CentroEducativo centroEducativo;


}
