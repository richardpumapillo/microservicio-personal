package com.softconsis.microservicio.app.personal.models.dao;

import com.softconsis.microservicio.app.personal.models.entity.EstadoCivil;
import org.springframework.data.repository.CrudRepository;

public interface EstadoCivilDao extends CrudRepository<EstadoCivil,Long> {
}
