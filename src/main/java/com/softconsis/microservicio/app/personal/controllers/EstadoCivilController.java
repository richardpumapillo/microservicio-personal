package com.softconsis.microservicio.app.personal.controllers;

import com.softconsis.microservicio.app.personal.models.entity.EstadoCivil;
import com.softconsis.microservicio.app.personal.models.service.IEstadoCivilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/estado_civil")
public class EstadoCivilController {
    @Autowired
    private final IEstadoCivilService estadoCivilService;

    public EstadoCivilController(IEstadoCivilService estadoCivilService) {
        this.estadoCivilService = estadoCivilService;
    }

    @GetMapping("/listar")
    public List<EstadoCivil> listar(){ return estadoCivilService.findAll();}

    @GetMapping("ver/{id}")
    public EstadoCivil detalle(@PathVariable Long id){ return estadoCivilService.findById(id);}

    @PostMapping("/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public EstadoCivil crear(@RequestBody EstadoCivil estadoCivil){ return estadoCivilService.save(estadoCivil);}

    @PutMapping("/editar/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public EstadoCivil editar(@RequestBody EstadoCivil estadoCivil,@PathVariable Long id){
        EstadoCivil estadoCivilDb= estadoCivilService.findById(id);

        estadoCivilDb.setNombreEstado(estadoCivil.getNombreEstado());

        return estadoCivilService.save(estadoCivilDb);
    }

    @DeleteMapping("/eliminar/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminar(@PathVariable Long id){ estadoCivilService.deleteById(id);}
}
