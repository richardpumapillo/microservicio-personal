package com.softconsis.microservicio.app.personal.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "centros_educativos")
public class CentroEducativo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected Long id;

    @Column(name = "nombre")
    protected String nombre;

    @OneToMany(mappedBy = "centroEducativo", cascade = CascadeType.ALL)
    protected Set<Capacitacion>  capacitaciones;

    @OneToMany(mappedBy = "centroEducativo", cascade = CascadeType.ALL)
    protected Set<Escolaridad> escolaridades;

    public CentroEducativo() {
        this.capacitaciones = new HashSet<Capacitacion>();
        this.escolaridades = new HashSet<Escolaridad>();
    }

    public CentroEducativo(String nombre) {
        this.nombre = nombre;
        this.capacitaciones = new HashSet<Capacitacion>();
        this.escolaridades = new HashSet<Escolaridad>();
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Capacitacion> getCapacitaciones() {
        return capacitaciones;
    }

    public void setCapacitaciones(Set<Capacitacion> capacitaciones) {
        this.capacitaciones = capacitaciones;
    }

    public Set<Escolaridad> getEscolaridades() {
        return escolaridades;
    }

    public void setEscolaridades(Set<Escolaridad> escolaridades) {
        this.escolaridades = escolaridades;
    }
}
