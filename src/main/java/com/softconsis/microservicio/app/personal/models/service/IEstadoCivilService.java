package com.softconsis.microservicio.app.personal.models.service;

import com.softconsis.microservicio.app.personal.models.entity.EstadoCivil;

import java.util.List;

public interface IEstadoCivilService {
    public List<EstadoCivil> findAll();
    public EstadoCivil findById(Long id);
    public EstadoCivil save(EstadoCivil estadoCivil);
    public void deleteById(Long id);
}
