package com.softconsis.microservicio.app.personal.controllers;

import com.softconsis.microservicio.app.personal.models.entity.Empleado;
import com.softconsis.microservicio.app.personal.models.service.IEmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

//@RequestMapping("/api/empleado")
@RestController
public class EmpleadoController {

	@Autowired
	private Environment env;
	
	@Value("${server.port}")
    private Integer port;
	
	@Autowired
	private final IEmpleadoService empleadoService;

    public EmpleadoController(IEmpleadoService empleadoService) {
        this.empleadoService = empleadoService;
    }

    @GetMapping("/listar")
    public List<Empleado> listar(){
        return empleadoService.findAll().stream().map(empleado ->{
        	empleado.setPort(Integer.parseInt(env.getProperty("local.server.port")));
        	//empleado.setPort(port);
        	return empleado;
        }).collect(Collectors.toList());
    	//return empleadoService.findAll();
    }

    @GetMapping("ver/{id}")
    public Empleado detalle(@PathVariable Long id){
        Empleado empleado = empleadoService.findById(id);
        empleado.setPort(Integer.parseInt(env.getProperty("local.server.port")));
        //empleado.setPort(port);
        return empleado;
    }

    @PostMapping("/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public Empleado crear(@RequestBody Empleado empleado){
        return empleadoService.save(empleado);
    }

    @PutMapping("/editar/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Empleado editar(@RequestBody Empleado empleado,@PathVariable Long id){
        Empleado empleadoDb= empleadoService.findById(id);

        empleadoDb.setNombre(empleado.getNombre());
        empleadoDb.setApellidoPaterno(empleado.getApellidoPaterno());
        empleadoDb.setDireccionCasa(empleado.getDireccionCasa());
        empleadoDb.setDireccionNacimiento(empleado.getDireccionNacimiento());

        return empleadoService.save(empleadoDb);
    }

    @DeleteMapping("/eliminar/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminar(@PathVariable Long id){
        empleadoService.deleteById(id);
    }

}
