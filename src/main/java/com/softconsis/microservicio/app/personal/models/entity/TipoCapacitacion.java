package com.softconsis.microservicio.app.personal.models.entity;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "tipos_capacitacion")
public class TipoCapacitacion implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected Long id;

    @Column(name = "descripcion")
    protected String descripcion;

    @OneToMany(mappedBy = "tipoCapacitacion",cascade = CascadeType.ALL)
    protected Set<Capacitacion> capacitaciones;

    public TipoCapacitacion() {
        this.capacitaciones = new HashSet<Capacitacion>();
    }

    public TipoCapacitacion(String descripcion) {
        this.descripcion = descripcion;
        this.capacitaciones = new HashSet<Capacitacion>();
    }

    public Long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set<Capacitacion> getCapacitaciones() {
        return capacitaciones;
    }

    public void setCapacitaciones(Set<Capacitacion> capacitaciones) {
        this.capacitaciones = capacitaciones;
    }
}
