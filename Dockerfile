FROM openjdk:11
VOLUME /tmp
EXPOSE 8001
ADD ./target/microservicio-personal-0.0.1-SNAPSHOT.jar microservicio-personal.jar
ENTRYPOINT ["java","-jar","/microservicio-personal.jar"]